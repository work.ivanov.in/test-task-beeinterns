// #6 Homework
const baseBlockCardTitle = document.querySelectorAll("h3.base-lvl");

baseBlockCardTitle.forEach((title) => {
  title.textContent = title.textContent.toUpperCase();
});

const baseBlockCardText = document.querySelectorAll("p.base-lvl");

baseBlockCardText.forEach((cardText) => {
  if (cardText.textContent.length >= 20) {
    cardText.textContent = cardText.textContent.slice(0, 20) + "...";
  }
});  

//#7 Homework
const menuItem = document.getElementsByClassName('toggle-item'),
      nestedMenuItems = document.querySelectorAll('ul.inactive'),
      menuArrows = document.querySelectorAll('.menu-toggle');

Array.from(menuItem).forEach( (item, i) => {
  item.addEventListener('click', () => {
    nestedMenuItems[i].classList.toggle('inactive');
    menuArrows[i].classList.toggle('rotate');
  });
});

// #8 Homework
const allLectionContainer = document.querySelector("#allLectures"),
  allLections = document.querySelectorAll(".lecture-item"),
  lectionCounterButton = document.querySelector("#lectionCounter"),
  filterButtons = document.querySelectorAll("[data-filter-type]");

let nodeBuffer, firstBlockLections;

window.onload = () => {
  lectionCounterButton.textContent = `${allLections.length} лекций`;

  Array.from(allLections).forEach((lection) => {
    nodeBuffer = lection.cloneNode(true);
    allLectionContainer.append(nodeBuffer);
  });

  firstBlockLections = allLectionContainer.children;
};
Array.from(filterButtons).forEach((button) => {
  button.addEventListener("click", () => {
    Array.from(firstBlockLections).forEach((lection) => {
      if (button.dataset.filterType === "All") {
        lection.classList.remove("inactive");
      } else if (button.dataset.filterType !== lection.dataset.filters) {
        lection.classList.toggle("inactive");
      }
    });
  });
});
